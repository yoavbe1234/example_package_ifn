from .main import is_odd, is_even

__all__ = ["is_odd", "is_even"]
