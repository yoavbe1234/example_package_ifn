def is_even(x: int):
    return x % 2 == 0


def is_odd(x: int):
    return x % 2 == 1
