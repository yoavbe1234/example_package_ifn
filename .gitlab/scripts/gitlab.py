import requests
import sys
import logging

BASE_URL = f"https://gitlab.com/api/v4/projects/"
token = ""
logging.basicConfig(level=logging.INFO)
current_version = "0.0.0"


def create_branch(branch_name):
    parameters = (
        f"{project_id}/repository/branches?branch={branch_name}&ref={main_branch}"
    )
    response = requests.post(BASE_URL + parameters, headers=headers)
    logging.info(response.text)


def create_mr(branch_name):
    parameters = f"{project_id}/merge_requests?title=Release{target_version}&source_branch={branch_name}&target_branch={main_branch}"
    response = requests.post(BASE_URL + parameters, headers=headers)
    logging.info(response.text)


def main():
    new_branch_name = f"release_v{target_version}"
    create_branch(new_branch_name)
    create_mr(new_branch_name)


if __name__ == "__main__":
    try:
        _, token, current_version, project_id, main_branch = sys.argv
    except ValueError:
        logging.error("Improper number of variables provided.")

    version_list = current_version.split(".")
    target_version = f"{version_list[0]}.{int(version_list[1])+1}.0"

    headers = {"PRIVATE-TOKEN": token}
    main()
