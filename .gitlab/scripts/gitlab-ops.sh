gitConfig(){
  git config --global user.email "${USERMAIL}"
  git config --global user.name "${USERNAME}"
}

gitlabAuth(){
  mkdir -p ~/.ssh
  echo "${SSH_KEY}" > ~/.ssh/id_rsa
  echo "${SSH_PUBLIC}" > ~/.ssh/id_rsa.pub
  chmod -0644 ~/.ssh/id_rsa
  eval `ssh-agent -s`
  ssh-add
  echo -e "Host *\n\tStrictHostKeyChecking no\n\n" >> ~/.ssh/config
  git remote set-url origin "git@gitlab.com:yoavbe1234/example_package_ifn.git"

  gitConfig
}

bumpversionMinor(){
  gitlabAuth
  pip install -U bumpversion
  bumpversion minor
  if (git push origin HEAD:${CI_COMMIT_REF_NAME} |& tee /tmp/bump_push_output.txt); then
    echo "bumpversion pushed"
  else
    cat /tmp/bump_push_output.txt
    exit 1
  fi;
}
